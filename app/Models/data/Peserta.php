<?php
/*
Coding ini di dedikasikan kepada Negara Republik Indonesia
untuk orang-orang yang peduli pada jaminan sosial ketenagakerjaan,
peduli pada tenaga kerja, buruh dan seluruh pekerja
- ditulis : Yan Kusyanto
- Tanggal : 02 Februari 2017
*/
namespace App\Models\data;

use Illuminate\Database\Eloquent\Model;
use DB;
use stdClass;

class peserta extends Model
{
protected $table="CAKUPAN";
protected $primaryKey = 'ID';

static function rekap($tahun,$bulan){

DB::enableQueryLog();
           
          
             $data = DB::table('CAKUPAN')       
                                -> join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('CAKUPAN.ID_SEGMEN as ID_SEGMEN','CAKUPAN.ID_PROGRAM as ID_PROGRAM','PROGRAM.PROGRAM as PROGRAM', DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH' ))
                                ->where('CAKUPAN.BULAN','=',$bulan)
                                ->where('CAKUPAN.TAHUN','=',$tahun)
                                ->orderBy('CAKUPAN.ID_SEGMEN', 'asc')
                                ->groupBy('CAKUPAN.ID_SEGMEN','CAKUPAN.ID_PROGRAM','PROGRAM.PROGRAM')
                                 ->get()->toJson();
    
 

return $data;  
       
}
static function rekapSegmen($tahun,$bulan){
    DB::enableQueryLog();
                 $data = DB::table('CAKUPAN')       
                                    ->join('SEGMEN', 'CAKUPAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                    ->select('CAKUPAN.ID_SEGMEN as ID_SEGMEN','SEGMEN.SEGMEN as SEGMEN', DB::raw('SUM(JUMLAH_PESERTA) as JUMLAHS' ))
                                    ->where('CAKUPAN.BULAN','=',$bulan)
                                    ->where('CAKUPAN.TAHUN','=',$tahun)
                                    ->where('CAKUPAN.ID_PROGRAM','=','1')
                                    ->orderBy('CAKUPAN.ID_SEGMEN', 'asc')
                                    ->groupBy('CAKUPAN.ID_SEGMEN','SEGMEN.SEGMEN')
                                    ->get()->toJson();    
    return $data;  
    }

static function rekapProgram($tahun,$bulan){
    DB::enableQueryLog();
                 $data = DB::table('CAKUPAN')       
                                    ->join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                    ->select('CAKUPAN.ID_PROGRAM as ID_PROGRAM','PROGRAM.PROGRAM as PROGRAM', DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH' ))
                                    ->where('CAKUPAN.BULAN','=',$bulan)
                                    ->where('CAKUPAN.TAHUN','=',$tahun)
                                    ->orderBy('CAKUPAN.ID_PROGRAM', 'asc')
                                    ->groupBy('CAKUPAN.ID_PROGRAM','PROGRAM.PROGRAM')
                                    ->get()->toJson();
    return $data;  
    }
    
static function rekapbu($tahun,$bulan){
      
    DB::enableQueryLog();
               
              
    $data = DB::table('CAKUPAN_BU')       
    -> join('SKALA_BU', 'CAKUPAN_BU.ID_SKALA', '=', 'SKALA_BU.ID_SKALA')
    -> join('PROGRAM','CAKUPAN_BU.ID_PROGRAM','=','PROGRAM.ID_PROGRAM')
    ->select('CAKUPAN_BU.ID_SKALA as ID_SKALA','SKALA_BU.SKALA','CAKUPAN_BU.ID_PROGRAM as ID_PROGRAM','PROGRAM.PROGRAM', DB::raw('SUM(JUMLAH_BU) as JUMLAH' ))
    ->where('CAKUPAN_BU.BULAN','=',$bulan)
    ->where('CAKUPAN_BU.TAHUN','=',$tahun)
    ->orderBy('CAKUPAN_BU.ID_SKALA', 'asc')
    ->groupBy('CAKUPAN_BU.ID_SKALA','SKALA_BU.SKALA','CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM')
    ->get()->toJson();

     /*                                // dd(DB::getQueryLog());                  
    dd($data);
    $json = json_encode($data); */
    
    return $data;  
           
    }
}
