<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Models\data\Peserta;
class PesertaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    static function rekapProgram(Request $request,$tahun,$bulan,$kanwil=null,$cabang=null){
      //  dd($request->path());
      //dd($bulan);
        $rekap=Peserta::rekapProgram($tahun,$bulan);
        return $rekap;

    }
    static function rekapSegmen(Request $request,$tahun,$bulan,$kanwil=null,$cabang=null){
        //  dd($request->path());
        //dd($bulan);
          $rekap=Peserta::rekapSegmen($tahun,$bulan);
          return $rekap;
  
      }
    static function rekapbu(Request $request,$tahun,$bulan,$kanwil=null,$cabang=null){
        //  dd($request->path());
        //dd($bulan);
          $rekap=Peserta::rekapbu($tahun,$bulan);
          return $rekap;
  
      }

    //
}
